var studentsAndPoints = [
    'Алексей Петров', 0,
    'Ирина Овчинникова', 60,
    'Глеб Стукалов', 30,
    'Антон Павлович', 30,
    'Виктория Заровская', 30,
    'Алексей Левенец', 70,
    'Тимур Вамуш', 30,
    'Евгений Прочан', 60,
    'Александр Малов', 0
];

// 1 Создаем конструктор студента
function Student(name, point) {
    this.name = name;
    this.point = point;
}
Student.prototype.show = function () {
    console.log("Студент %s набрал %s баллов", this.name, this.point);
};
// 2 Создаем конструктор группы
function StudentList(title, arr) {
    this.titleGroup = title;
    if (arr !== undefined){
        for (var i = 0, len = arr.length; i < len; i += 2){
            this.add(arr[i], arr[i + 1]);
        }
    }

}
StudentList.prototype = Object.create(Array.prototype);
StudentList.prototype.constructor = StudentList;
StudentList.prototype.add = function (name, point) {
                                if (name && (point >= 0)){
                                    this.push(new Student(name,point));
                                }
                             };
// 3 пункт
var hj2 = new StudentList("HJ-2",studentsAndPoints);
hj2.add("Николя Саркози", 0);
hj2.add("Ангела Меркель", 10);
// 4 пункт
var html7 = new StudentList("HTML-7");
html7.add("Владимир Путин", 100);
html7.add("Дмитрий Медведев", 90);

// 5 пункт
StudentList.prototype.show = function () {
    console.log("Группа %s (%d студентов)", this.titleGroup, this.length);
    for (var i = 0, len = this.length; i < len; i++){
        this[i].show();
    }
};
html7.show();
hj2.show();

// 7. Перевести студента
html7.push(hj2.splice(9, 1)[0]);
// ЕСЛИ ПЕРЕВОДИТЬ НЕСКОЛЬКО студентов то понадобиться forEach
// hj2.splice(9, 2).forEach(function (studentObj, i) {
//     html7.push(studentObj);
// });






// function transfer(from, where, student) {
//     var fromGroup = from;
//     var whereGroup = where;
//     var studentTransfer;
//     fromGroup.forEach(function (value, i) {
//         if (value.name === student) {
//             studentTransfer = fromGroup.splice(i, 1);
//             whereGroup.push(studentTransfer[0]);
//         }
//     });
// }
// transfer(hj2.outputGroup, html7.outputGroup, "Павел Усов");
// console.log("***transfer***");
// hj2.show();
// html7.show();