var studentsAndPoints = [
    'Алексей Петров', 0,
    'Ирина Овчинникова', 60,
    'Глеб Стукалов', 30,
    'Антон Павлович', 30,
    'Виктория Заровская', 30,
    'Алексей Левенец', 70,
    'Тимур Вамуш', 30,
    'Евгений Прочан', 60,
    'Александр Малов', 0
];

// 1 Создаем конструктор студента
function Student(name, point) {
    this.name = name;
    this.point = point;
}
Student.prototype.show = function () {
    console.log("Студент %s набрал %s баллов", this.name, this.point);
};
// 2 Создаем конструктор группы
function StudentList(title, arr) {
    this.titleGroup = title;
    this.arrGroup = arr; // Переменная которая будет принимать массив значений
}
StudentList.prototype = Array.prototype;
StudentList.prototype.add = function (nameStudent, pointStudent) {
    this.arrGroup.push(nameStudent, pointStudent);

};
// 3 Создать список HJ-2
var hj2 = new StudentList("HJ-2", studentsAndPoints);
// 4 Добавить студентов
hj2.add("Павел Усов", 20);
hj2.add("Олег Попов", 30);
hj2.add("Иван Зыкин", 0);
// 5 Создать группу HTML-7
var html7 = new StudentList("HTML-7", []);
html7.add("Марат Башаров", 20);
html7.add("Олег Газманов", 50);
// 6 Добавить метод  show
StudentList.prototype.show = function () {
    console.log("Группа %s (%d студентов):", this.titleGroup, this.arrGroup.length/2);
    this.arrGroup.forEach(function (value, i, arr) {
        if (i % 2 == 0){
            console.log("Студент %s набрал %d баллов", value, arr[i+1]);
        }
    });
};
hj2.show();
html7.show();
// 7. Перевести студента
function transfer(from, where, student) {
    var fromGroup = from;
    var whereGroup = where;
    var stud;
    fromGroup.forEach(function (value, i) {
        if (value == student){
            stud = fromGroup.splice(i, 2);
            whereGroup.push(stud[0], stud[1]);
        }
    });
}
transfer(hj2.arrGroup, html7.arrGroup, "Павел Усов");
html7.show();