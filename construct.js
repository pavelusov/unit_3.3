/**
 * Created by pavelusov on 21.09.16.
 */
function Book(title, author, price, year, amount) {
    this.title = title;
    this.author = author;
    this.price = price;
    this.year = year;
    this.amount = amount;
    this.show = function () {
        console.log("Эта книга: %s. Автор: %s. Цена: %s рублей. Год: %s. На складе %s штук.",
        this.title, this.author, this.price, this.year, this.amount);
    }
}

var gogol = new Book("Мертвые души", "Гоголь", 200, 1984, 14);
gogol.show();

// Создаем книжную полку (массив книг).
var books = [];
// Ставим книги на полку
books.push(new Book("JavaScript для профи", "Закас", 1200, 2015, 5));
books.push(new Book("jQuery", "Фримен", 1900, 2015, 50));
books[0].show();
books[1].show();
// Создаем одну книгу (объект)
var jsbook = {};
Book.call(jsbook, "JavaScript", "Закас", 1200, 2015, 15);
jsbook.show();
console.log(jsbook.constructor); // [Function: Object]
console.log(books[0].constructor); // [Function: Book]

// Универсальный конструктор. Заглушка  return
function Newspaper(title, price) {
    // Заглушка. На случай если конструктор вызовут без new.
    if (this === undefined || this.constructor !== Newspaper){
        return new Newspaper(title, price);
    }
    this.title = title;
    this.price = price;
}
// Проверяем
var pravda = new Newspaper("Правда", 20);
var vestnik = Newspaper("Вестник", 33);
console.log(pravda.constructor); // [Function: Newspaper]
console.log(vestnik.constructor); // [Function: Newspaper]

// Метод BIND
name = "Lenin"; // window.name
function helloUser() {
    console.log("Привет " + this.name);
}
helloUser(); // Привет Lenin - this.name указывает на window.name
var user = { name : "Pavel"};
helloUser.call(user); // Привет Pavel - Вызываем функцию hellouser для user
var bindUser = helloUser.bind(user); // Привязываем контекст this к объекту user
bindUser.call({name : "Dima"}); // Привет Pavel - Жестко привязали this
